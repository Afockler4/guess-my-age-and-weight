from random import randint
import Constants

def winGame():
  print(Constants.VICTORY_MESSAGE)
  exit()

def giveUp():
  print(Constants.GIVE_UP)
  print(Constants.GAME_OVER)
  exit()

# ======================= GAME =============================================
def game():
  
  print("I am going to try to guess your age and weight!")
  for i in range(5):
    years_old = randint(1,90)
    weight = randint(100,300)
    print("Are you {} years old and {} pounds body weight?".format(years_old, weight))
    answer = input("Select yes or no: ")
    if answer == "yes":
      winGame()
    elif i < 4:
      print("Hmm alright, I'll try again..")
    else:
      print("Okay I'm out of guesses..")
  start_over = input("Would you like me to try again? Select yes or no: ")
  if start_over == "yes":
    game()
  else:
    giveUp()
# ======================= GAME ============================================

print("Hello!")

response = input("Would you like to play a game? Select yes or no: ")

if response == "yes":
  print("Okay let's get started!")
  name = input(Constants.ASK_NAME)
  print("Hello, {}!".format(name))
  game()

else:
  print("Wow you're no fun :(")
  print("GAME OVER")
  exit()




